package board.service;

import java.util.*;

import board.dto.*;

public interface BoardService {

	List<BoardDto> selectBoardList() throws Exception;
	
}
