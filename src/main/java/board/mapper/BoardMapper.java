package board.mapper;

import java.util.*;

import org.apache.ibatis.annotations.*;

import board.dto.*;

@Mapper
public interface BoardMapper {

	List<BoardDto> selectBoardList() throws Exception;
	
}
